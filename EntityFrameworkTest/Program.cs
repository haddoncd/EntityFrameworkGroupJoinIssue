﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EntityFrameworkTest
{
    class Program
    {
        public class BloggingContext : DbContext
        {
            public static readonly LoggerFactory LoggerFactory
                = new LoggerFactory(new[] { new DebugLoggerProvider() });

            public DbSet<Blog> Blogs { get; set; }
            public DbSet<Post> Posts { get; set; }
            public DbSet<Subscription> Subscriptions { get; set; }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                optionsBuilder
                    .UseSqlite("Data Source=blogging.db")
                    .UseLoggerFactory(LoggerFactory);
            }
        }

        public class Blog
        {
            public int BlogId { get; set; }
            public string BlogTitle { get; set; }

            public List<Post> Posts { get; set; }
            public List<Subscription> Subscriptions { get; set; }
        }

        public class Post
        {
            public int PostId { get; set; }
            public string PostTitle { get; set; }

            public int BlogId { get; set; }
            public Blog Blog { get; set; }
        }

        public class Subscription
        {
            public int SubscriptionId { get; set; }

            public int BlogId { get; set; }
            public Blog Blog { get; set; }
        }

        static void Main(string[] args)
        {
            using (var db = new BloggingContext())
            {
                {
                    Console.WriteLine("Resetting database...");

                    db.Blogs.RemoveRange(db.Blogs);
                    db.Posts.RemoveRange(db.Posts);
                    db.Subscriptions.RemoveRange(db.Subscriptions);

                    var count = db.SaveChanges();
                    Console.WriteLine("Ok! ({0} records affected)", count);
                }

                Console.WriteLine();

                {
                    Console.WriteLine("Adding data...");
                    
                    for (int i = 0; i < 2; ++i)
                    {
                        var blogEntry = db.Blogs.Add(new Blog { BlogTitle = "Blog " + i });

                        db.Subscriptions.Add(new Subscription { BlogId = blogEntry.Entity.BlogId });

                        db.Posts.Add(new Post { PostTitle = "Post 0", BlogId = blogEntry.Entity.BlogId });
                        db.Posts.Add(new Post { PostTitle = "Post 1", BlogId = blogEntry.Entity.BlogId });
                    }

                    var count = db.SaveChanges();
                    Console.WriteLine("Ok! ({0} records affected)", count);
                }

                Console.WriteLine();

                Console.WriteLine("All blogs:");
                foreach (var blog in db.Blogs
                    .GroupJoin(
                        db.Posts,
                        b => b.BlogId,
                        p => p.BlogId,
                        (b, ps) => new
                        {
                            b.BlogId,
                            b.BlogTitle,
                            PostTitles = ps.Select(p => p.PostTitle),
                        }
                    )
                    .OrderBy(x => x.BlogId))
                {
                    Console.WriteLine(" - {0}", blog.BlogTitle);
                    foreach (var postTitle in blog.PostTitles)
                    {
                        Console.WriteLine("    - {0}", postTitle);
                    }
                }

                Console.WriteLine();

                Console.WriteLine("All subscribed blogs:");
                foreach (var blog in db.Subscriptions
                    .Join(
                        db.Blogs,
                        s => s.BlogId,
                        b => b.BlogId,
                        (s, b) => new
                        {
                            s.SubscriptionId,
                            b.BlogId,
                            b.BlogTitle,
                        }
                    )
                    .GroupJoin(
                        db.Posts,
                        x => x.BlogId,
                        p => p.BlogId,
                        (x, ps) => new
                        {
                            x.SubscriptionId,
                            x.BlogTitle,
                            PostTitles = ps.Select(p => p.PostTitle),
                        }
                    )
                    .OrderBy(x => x.SubscriptionId))
                {
                    Console.WriteLine(" - {0}", blog.BlogTitle);
                    foreach (var postTitle in blog.PostTitles)
                    {
                        Console.WriteLine("    - {0}", postTitle);
                    }
                }
            }

#if DEBUG
            // Keep the window open so we can see the output if we're running in VS
            if (System.Diagnostics.Debugger.IsAttached) System.Diagnostics.Debugger.Break();
#endif
        }
    }
}
