﻿// <auto-generated />
using EntityFrameworkTest;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EntityFrameworkTest.Migrations
{
    [DbContext(typeof(Program.BloggingContext))]
    [Migration("20180909164402_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.2-rtm-30932");

            modelBuilder.Entity("EntityFrameworkTest.Program+Blog", b =>
                {
                    b.Property<int>("BlogId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BlogTitle");

                    b.HasKey("BlogId");

                    b.ToTable("Blogs");
                });

            modelBuilder.Entity("EntityFrameworkTest.Program+Post", b =>
                {
                    b.Property<int>("PostId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BlogId");

                    b.Property<string>("PostTitle");

                    b.HasKey("PostId");

                    b.HasIndex("BlogId");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("EntityFrameworkTest.Program+Subscription", b =>
                {
                    b.Property<int>("SubscriptionId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BlogId");

                    b.HasKey("SubscriptionId");

                    b.HasIndex("BlogId");

                    b.ToTable("Subscriptions");
                });

            modelBuilder.Entity("EntityFrameworkTest.Program+Post", b =>
                {
                    b.HasOne("EntityFrameworkTest.Program+Blog", "Blog")
                        .WithMany("Posts")
                        .HasForeignKey("BlogId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("EntityFrameworkTest.Program+Subscription", b =>
                {
                    b.HasOne("EntityFrameworkTest.Program+Blog", "Blog")
                        .WithMany("Subscriptions")
                        .HasForeignKey("BlogId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
